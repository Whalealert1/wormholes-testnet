<p align="center">
  <img width="300" height="auto" src="https://user-images.githubusercontent.com/108969749/201534786-9fd914e1-fe09-456f-b56a-4082da2ae687.jpeg">
</p>


### Spesifikasi Hardware :
NODE  | CPU     | RAM      | SSD     |
| ------------- | ------------- | ------------- | -------- |
| Testnet | 4          | 8         | 256  |

### Install docker engine
```
sudo apt update && sudo apt upgrade -y
sudo apt install curl build-essential git wget jq make gcc ack tmux ncdu -y
sudo apt-get install ca-certificates curl gnupg lsb-release -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
apt install docker.io -y
```
### Run node
```
wget -O wormholes.sh https://raw.githubusercontent.com/Whalealert/wormholes-testnet/main/wormholes.sh && chmod +x wormholes.sh && ./wormholes.sh
```
Hasil :
> Enter your private key：

### Monitor node
```
wget -O monitor.sh https://raw.githubusercontent.com/Whalealert/wormholes-testnet/main/monitor.sh && chmod +x monitor.sh && ./monitor.sh
```

### Next step ? Become A Validator
`
*** membutuhkan 70k ERB untuk menjadi validator ***`

<a href="https://wormholes.com/docs/Install/stake/index.html"> OFFICIAL <a/>

### CommandLine
 * check versi node
```
curl -X POST -H "Content-Type:application/json" --data '{"jsonrpc":"2.0","method":"eth_version","id":64}' http://127.0.0.1:8545
```
 * check koneksi node
```
curl -X POST -H 'Content-Type:application/json' --data '{"jsonrpc":"2.0","method":"net_peerCount","id":1}' http://127.0.0.1:8545
```
 * check log wormholes
```
tail -f /wm/.wormholes/wormholes.log | grep -i 'your adress'
```
  * check private key
```
cat /wm/.wormholes/wormholes/nodekey
```
### Stop node

```
docker stop wormholes && docker rm wormholes
```
